'use strict';

angular.module('testApp')
       .service('ResourcesService', ResourcesService);

function ResourcesService($resource, apiEndpoints) 
{
    var ResourcesService = this;

    var userDataResource       = $resource(apiEndpoints.userData);

    ResourcesService.login = function() {
        return userDataResource.get();
    };

};