'use strict';

angular.module('testApp')
  .controller('BaseCtrl', function ($scope,$state,AuthService) {
    if( $state.current.name == "base")
    {
        $state.go("base.login");
    }

    var baseCtrl = this;
    baseCtrl.UserData = AuthService.getUserData();

    AuthService.subscribeOnUserDataChanges($scope, function(){
      baseCtrl.UserData = AuthService.getUserData();
    });

    baseCtrl.logout = function() {
      if(confirm("Are you sure?"))
      {
          AuthService.logout();
          $state.go('base.login');
      }
    };
  });