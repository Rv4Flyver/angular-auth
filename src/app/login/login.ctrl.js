'use strict';

angular.module('testApp')
  .controller('LoginCtrl', function ($scope,$state,AuthService) {
    if(AuthService.checkIfAuthorized()) {
      $state.go('base.success');
    }

    var LoginCtrl = this;
    LoginCtrl.UserCredentials = AuthService.getUserData();
   
    LoginCtrl.login = function() {
      AuthService.login(LoginCtrl.UserCredentials);
    }
  });