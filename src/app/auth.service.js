'use strict';

angular.module('testApp')
	   .service('AuthService', AuthService);

/** @ngInject */
function AuthService($rootScope, $q, $state, ResourcesService, localStorageService)
{
    var AuthService = this;

/**************************************************
 * Security Check Related Section
 **************************************************/
    var userData = {
      login : '',
      password : '',
      remember: false
    };
    var authorized = false ; 

    // Events Subscription & Handling
    AuthService.subscribeOnUserDataChanges = function(scope, callback) {
            var handler = $rootScope.$on('security-service-user-data-event', callback);
            scope.$on('$destroy', handler);
        };

    function notifyAboutUserDataChanges() {
        $rootScope.$emit('security-service-user-data-event');
    }

    // Check if user authorized
    AuthService.checkIfAuthorized = function() {

        console.log("+AuthService.checkIfAuthorized: " + (authorized));
        return authorized;
    };

    // Get User Data
    AuthService.getUserData = function() {
        return userData;
    };

    /***************************************************
     * Login
     ***************************************************/
    AuthService.login = function(UserCredentials) {
       var deferredObj = $q.defer();
       ResourcesService.login().$promise.then(
           function(response){ // success

                if(typeof response.login !== 'undefined' 
                   && typeof response.password !== 'undefined')
                {
                    if(UserCredentials.login == response.login 
                        && UserCredentials.password == response.password)
                    {
                        console.log('+ LoggedIn: success:' + response);
                        authorized = true;
  
                        UpdateUserData(response);

                        if(UserCredentials.remember) 
                        {
                            localStorageService.set('rvch_testApp',UserCredentials);
                        }
                        deferredObj.resolve(response);
                        
                        $state.go('base.success');
                    } 
                    else
                    {
                        deferredObj.resolve({});
                        $state.go('base.error');
                    }
                }
           },
           function(response) { // fail
                console.log('+ LogIn failed: ' + response);
                deferredObj.reject(response);
                $state.go('base.error');
           }
        );
        return deferredObj.promise;
    };

    AuthService.logout = function() { 
        userData = {
            login :   ''
        };
        authorized = false;
        localStorageService.remove('rvch_testApp');
        notifyAboutUserDataChanges();
    }

    function UpdateUserData(data) 
    {
        userData.login = data.login;
        notifyAboutUserDataChanges();
    }

    /***************************************************
     * Session Restoration (Remember me)
     ***************************************************/
    if(localStorageService.get('rvch_testApp') != null) {
        AuthService.login(localStorageService.get('rvch_testApp'));
        console.log('+ RestoreSession ended.');
    }
}
