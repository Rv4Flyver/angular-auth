'use strict';

// Declare app level module which depends on views, and components
angular.module('testApp', ['ui.router','ngResource','LocalStorageModule'])
  .config(function ($stateProvider, $urlRouterProvider, $provide,$locationProvider) {
    
    // R O U T I N G
    $stateProvider
      .state('base', {
        url: '',
        templateUrl: 'app/base/base.view.html',
        controller: 'BaseCtrl',
        controllerAs: 'baseCtrl'
      })
        .state('base.login', {
          url: '/login',
          templateUrl: 'app/login/login.view.html',
          controller: 'LoginCtrl',
          controllerAs: 'loginCtrl'
        })
        .state('base.success', {
          url: '/success',
          templateUrl: 'app/success/success.view.html',
          controller: 'SuccessCtrl',
          controllerAs: 'successCtrl'
        })
        .state('base.error', {
          url: '/error',
          templateUrl: 'app/error/error.view.html',
          controller: 'ErrorCtrl',
          controllerAs: 'errorCtrl'
        });
    $urlRouterProvider.otherwise('/login');

    $locationProvider.hashPrefix('!');
  });
