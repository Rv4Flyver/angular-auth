'use strict';

angular.module('testApp')
  .controller('SuccessCtrl', function ($scope,$state,AuthService) {
    if(!AuthService.checkIfAuthorized()) {
      $state.go('base.error');
    }
  });